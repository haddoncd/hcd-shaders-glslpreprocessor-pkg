#ifdef glslPreprocessor_cpp
#error Multiple inclusion
#endif
#define glslPreprocessor_cpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef Str_hpp
#error "Please include Str.hpp before this file"
#endif

#ifndef StrBuf_hpp
#error "Please include StrBuf.hpp before this file"
#endif

#ifndef print_hpp
#error "Please include print.hpp before this file"
#endif

#ifndef File_hpp
#error "Please include File.hpp before this file"
#endif

#ifndef filesystem_hpp
#error "Please include filesystem.hpp before this file"
#endif

#ifndef PageAllocator_hpp
#error "Please include PageAllocator.hpp before this file"
#endif

#ifndef List_hpp
#error "Please include List.hpp before this file"
#endif

#ifndef __glcorearb_h_
#error "Please include GL/glcorearb.h before this file"
#endif

#ifndef _COMPILER_INTERFACE_INCLUDED_
#error "Please include ShaderLang.h before this file"
#endif

#ifndef _LOCAL_INTERMEDIATE_INCLUDED_
#error "Please include localintermediate.h before this file"
#endif


// TODOs:
//
// 1.  Support structs in uniform variables (See todo in
//     ShaderInputTraverser::visitSymbol).
//
// 2.  Support sharing block data between multiple shaders by allowing the
//     underlying buffer to be bound to both. This might become important
//     since SSBOs are writable from the shader.
//
// 3.  Support a single shader having multiple blocks using the same
//     structure, probably by automatically eliding a single-field-struct
//     wrapping them into a block.
//


// Copied from standalone glslang validator
namespace glslang
{
  const TBuiltInResource DefaultTBuiltInResource =
  {
    /* .MaxLights = */ 32,
    /* .MaxClipPlanes = */ 6,
    /* .MaxTextureUnits = */ 32,
    /* .MaxTextureCoords = */ 32,
    /* .MaxVertexAttribs = */ 64,
    /* .MaxVertexUniformComponents = */ 4096,
    /* .MaxVaryingFloats = */ 64,
    /* .MaxVertexTextureImageUnits = */ 32,
    /* .MaxCombinedTextureImageUnits = */ 80,
    /* .MaxTextureImageUnits = */ 32,
    /* .MaxFragmentUniformComponents = */ 4096,
    /* .MaxDrawBuffers = */ 32,
    /* .MaxVertexUniformVectors = */ 128,
    /* .MaxVaryingVectors = */ 8,
    /* .MaxFragmentUniformVectors = */ 16,
    /* .MaxVertexOutputVectors = */ 16,
    /* .MaxFragmentInputVectors = */ 15,
    /* .MinProgramTexelOffset = */ -8,
    /* .MaxProgramTexelOffset = */ 7,
    /* .MaxClipDistances = */ 8,
    /* .MaxComputeWorkGroupCountX = */ 65535,
    /* .MaxComputeWorkGroupCountY = */ 65535,
    /* .MaxComputeWorkGroupCountZ = */ 65535,
    /* .MaxComputeWorkGroupSizeX = */ 1024,
    /* .MaxComputeWorkGroupSizeY = */ 1024,
    /* .MaxComputeWorkGroupSizeZ = */ 64,
    /* .MaxComputeUniformComponents = */ 1024,
    /* .MaxComputeTextureImageUnits = */ 16,
    /* .MaxComputeImageUniforms = */ 8,
    /* .MaxComputeAtomicCounters = */ 8,
    /* .MaxComputeAtomicCounterBuffers = */ 1,
    /* .MaxVaryingComponents = */ 60,
    /* .MaxVertexOutputComponents = */ 64,
    /* .MaxGeometryInputComponents = */ 64,
    /* .MaxGeometryOutputComponents = */ 128,
    /* .MaxFragmentInputComponents = */ 128,
    /* .MaxImageUnits = */ 8,
    /* .MaxCombinedImageUnitsAndFragmentOutputs = */ 8,
    /* .MaxCombinedShaderOutputResources = */ 8,
    /* .MaxImageSamples = */ 0,
    /* .MaxVertexImageUniforms = */ 0,
    /* .MaxTessControlImageUniforms = */ 0,
    /* .MaxTessEvaluationImageUniforms = */ 0,
    /* .MaxGeometryImageUniforms = */ 0,
    /* .MaxFragmentImageUniforms = */ 8,
    /* .MaxCombinedImageUniforms = */ 8,
    /* .MaxGeometryTextureImageUnits = */ 16,
    /* .MaxGeometryOutputVertices = */ 256,
    /* .MaxGeometryTotalOutputComponents = */ 1024,
    /* .MaxGeometryUniformComponents = */ 1024,
    /* .MaxGeometryVaryingComponents = */ 64,
    /* .MaxTessControlInputComponents = */ 128,
    /* .MaxTessControlOutputComponents = */ 128,
    /* .MaxTessControlTextureImageUnits = */ 16,
    /* .MaxTessControlUniformComponents = */ 1024,
    /* .MaxTessControlTotalOutputComponents = */ 4096,
    /* .MaxTessEvaluationInputComponents = */ 128,
    /* .MaxTessEvaluationOutputComponents = */ 128,
    /* .MaxTessEvaluationTextureImageUnits = */ 16,
    /* .MaxTessEvaluationUniformComponents = */ 1024,
    /* .MaxTessPatchComponents = */ 120,
    /* .MaxPatchVertices = */ 32,
    /* .MaxTessGenLevel = */ 64,
    /* .MaxViewports = */ 16,
    /* .MaxVertexAtomicCounters = */ 0,
    /* .MaxTessControlAtomicCounters = */ 0,
    /* .MaxTessEvaluationAtomicCounters = */ 0,
    /* .MaxGeometryAtomicCounters = */ 0,
    /* .MaxFragmentAtomicCounters = */ 8,
    /* .MaxCombinedAtomicCounters = */ 8,
    /* .MaxAtomicCounterBindings = */ 1,
    /* .MaxVertexAtomicCounterBuffers = */ 0,
    /* .MaxTessControlAtomicCounterBuffers = */ 0,
    /* .MaxTessEvaluationAtomicCounterBuffers = */ 0,
    /* .MaxGeometryAtomicCounterBuffers = */ 0,
    /* .MaxFragmentAtomicCounterBuffers = */ 1,
    /* .MaxCombinedAtomicCounterBuffers = */ 1,
    /* .MaxAtomicCounterBufferSize = */ 16384,
    /* .MaxTransformFeedbackBuffers = */ 4,
    /* .MaxTransformFeedbackInterleavedComponents = */ 64,
    /* .MaxCullDistances = */ 8,
    /* .MaxCombinedClipAndCullDistances = */ 8,
    /* .MaxSamples = */ 4,
    /* .limits = */
    {
      /* .nonInductiveForLoops = */ 1,
      /* .whileLoops = */ 1,
      /* .doWhileLoops = */ 1,
      /* .generalUniformIndexing = */ 1,
      /* .generalAttributeMatrixVectorIndexing = */ 1,
      /* .generalVaryingIndexing = */ 1,
      /* .generalSamplerIndexing = */ 1,
      /* .generalVariableIndexing = */ 1,
      /* .generalConstantMatrixVectorIndexing = */ 1,
    }
  };

  Str nameOfTStorageQualifier(TStorageQualifier storage)
  {
    switch (storage)
    {
      case EvqTemporary:     return "Temporary"_s;
      case EvqGlobal:        return "Global"_s;
      case EvqConst:         return "Const"_s;
      case EvqVaryingIn:     return "VaryingIn"_s;
      case EvqVaryingOut:    return "VaryingOut"_s;
      case EvqUniform:       return "Uniform"_s;
      case EvqBuffer:        return "Buffer"_s;
      case EvqShared:        return "Shared"_s;
      case EvqIn:            return "In"_s;
      case EvqOut:           return "Out"_s;
      case EvqInOut:         return "InOut"_s;
      case EvqConstReadOnly: return "ConstReadOnly"_s;
      case EvqVertexId:      return "VertexId"_s;
      case EvqInstanceId:    return "InstanceId"_s;
      case EvqPosition:      return "Position"_s;
      case EvqPointSize:     return "PointSize"_s;
      case EvqClipVertex:    return "ClipVertex"_s;
      case EvqFace:          return "Face"_s;
      case EvqFragCoord:     return "FragCoord"_s;
      case EvqPointCoord:    return "PointCoord"_s;
      case EvqFragColor:     return "FragColor"_s;
      case EvqFragDepth:     return "FragDepth"_s;
      case EvqLast:          return "Last"_s;
      default:               return "Unknown"_s;
    }
  }
}

namespace glslPreprocessor
{
  namespace primitiveType
  {
    enum Enum
    {
      UNSUPPORTED = -1,
      U8,
      B32,
      I32,
      I64,
      U32,
      U64,
      F32,
      F64,
      ENUM_COUNT
    };

    size_t size[] =
    {
      1,
      4,
      4,
      8,
      4,
      8,
      4,
      8
    };

    Str name[] =
    {
      "u8"_s,
      "b32"_s,
      "i32"_s,
      "i64"_s,
      "u32"_s,
      "u64"_s,
      "f32"_s,
      "f64"_s
    };

    Str glEnumName(Enum e)
    {
      switch (e)
      {
        case U8:  return "GL_UNSIGNED_BYTE"_s;
        case I32: return "GL_INT"_s;
        case U32: return "GL_UNSIGNED_INT"_s;
        case F32: return "GL_FLOAT"_s;
        case F64: return "GL_DOUBLE"_s;
        default:
          assert(false);
          return Str::empty();
      }
    };

    Str glUniformTypeSuffix(Enum e)
    {
      switch (e)
      {
        case I32: return "i"_s;
        case U32: return "ui"_s;
        case F32: return "f"_s;
        default:
          assert(false);
          return Str::empty();
      }
    }

    Enum fromShaderType(glslang::TBasicType basic_type)
    {
      switch (basic_type)
      {
        case glslang::EbtBool:   return primitiveType::B32;
        case glslang::EbtInt:    return primitiveType::I32;
        case glslang::EbtInt64:  return primitiveType::I64;
        case glslang::EbtUint:   return primitiveType::U32;
        case glslang::EbtUint64: return primitiveType::U64;
        case glslang::EbtFloat:  return primitiveType::F32;
        case glslang::EbtDouble: return primitiveType::F64;
        default:                 return primitiveType::UNSUPPORTED;
      }
    }
  }

  namespace vectorType
  {
    enum Enum
    {
      UNSUPPORTED = -1,
      PRIMITIVE,
      V2,
      V3,
      V4,
      M2,
      M3,
      M4,
      ENUM_COUNT
    };

    bool isValid(Enum e)
    {
      return UNSUPPORTED < e && e < ENUM_COUNT;
    }

    Str name[] =
    {
      Str::empty(),
      "V2"_s,
      "V3"_s,
      "V4"_s,
      "M2"_s,
      "M3"_s,
      "M4"_s
    };

    u32 cols[] =
    {
      1,
      1,
      1,
      1,
      2,
      3,
      4
    };

    u32 rows[] =
    {
      1,
      2,
      3,
      4,
      2,
      3,
      4
    };

    u32 count(Enum e)
    {
      return cols[e] * rows[e];
    };

    bool isVector(Enum e)
    {
      return isValid(e) && (cols[e] == 1);
    }

    bool isMatrix(Enum e)
    {
      return isValid(e) && (2 <= cols[e]);
    }

    bool isSquare(Enum e)
    {
      return isValid(e) && (rows[e] == cols[e]);
    }

    Enum v(u32 dims)
    {
      switch (dims)
      {
        case  1: return PRIMITIVE;
        case  2: return V2;
        case  3: return V3;
        case  4: return V4;
        default: return UNSUPPORTED;
      }
    }

    Enum m(u32 m_cols, u32 m_rows)
    {
      switch (m_cols)
      {
        case 1: return v(m_rows);

        case 2:
        {
          switch (m_rows)
          {
            case 2: return M2;
            default: return UNSUPPORTED;
          }
        } break;

        case 3:
        {
          switch (m_rows)
          {
            case 3: return M3;
            default: return UNSUPPORTED;
          }
        } break;

        case 4:
        {
          switch (m_rows)
          {
            case 4: return M4;
            default: return UNSUPPORTED;
          }
        } break;

        default: return UNSUPPORTED;
      }
    }
  }

  struct StructType;

  namespace typeType
  {
    enum Enum
    {
      VALUE,
      STRUCT,
      SAMPLER,
      ENUM_COUNT
    };
  }

  struct Type
  {
    typeType::Enum type;
    u32 arrayDims;

    union
    {
      struct
      {
        primitiveType::Enum pType;
        vectorType::Enum    vType;
      };

      StructType *sType;
      Str         samplerDesc;
    };

    u64 arraySizes[];

    static size_t sizeWith(u32 array_dims)
    {
      return offsetOf(Type, arraySizes[array_dims]);
    }

    void setSize(u32 array_dims)
    {
      arrayDims = array_dims;
    }

    NO_COPY_OR_MOVE(Type)

    size_t arrayTotalSize()
    {
      size_t result = 1;
      for (u32 i = 0; i < arrayDims; ++i) result *= arraySizes[i];
      return result;
    }

    size_t size();
  };

  namespace structSizeType
  {
    enum Enum
    {
      FIXED,
      ARRAY,
      STRUCT
    };
  }

  struct StructType
  {
    struct Member
    {
      size_t offset;
      Type  *type;
      Str    name;
    };

    bool   complete;
    Str    name;
    size_t size;
    u64    memberCount;
    Member members[];

    static size_t sizeWith(u64 count)
    {
      return offsetOf(StructType, members[count]);
    }

    void setSize(u64 count)
    {
      memberCount = count;
    }

    NO_COPY_OR_MOVE(StructType)

    structSizeType::Enum sizeType()
    {
      structSizeType::Enum result;

      Member *final_member = &members[memberCount-1];

      if (final_member->type->arrayTotalSize() == 0)
      {
        result = structSizeType::ARRAY;
      }
      else if ((final_member->type->type == typeType::STRUCT) &&
               (final_member->type->sType->sizeType() != structSizeType::FIXED))
      {
        result = structSizeType::STRUCT;
      }
      else
      {
        result = structSizeType::FIXED;
      }

      return result;
    }
  };

  size_t Type::size()
  {
    size_t base_size;

    if (type == typeType::STRUCT)
    {
      base_size = sType->size;
    }
    else
    {
      base_size = primitiveType::size[pType] *
                  vectorType::count(vType);
    }

    return base_size * arrayTotalSize();
  }

  struct ValueTypePrintCommand
  {
    primitiveType::Enum pType;
    vectorType::Enum    vType;
  };

  ValueTypePrintCommand printValueType(primitiveType::Enum p_type,
                                       vectorType::Enum    v_type)
  {
    return ValueTypePrintCommand { p_type, v_type };
  }

  ValueTypePrintCommand printValueType(Type *type)
  {
    return ValueTypePrintCommand { type->pType, type->vType };
  }

  struct TypeDeclarationPrintCommand
  {
    Type *type;
    Str   name;
  };

  TypeDeclarationPrintCommand printTypeDeclaration(Type *type,
                                                   Str   name)
  {
    return TypeDeclarationPrintCommand { type, name };
  }
}

template <typename S>
bool printValueTypeName(S out, glslPreprocessor::primitiveType::Enum p_type,
                               glslPreprocessor::vectorType::Enum    v_type)
{
  using namespace glslPreprocessor;
  bool result = true;

  if (v_type != vectorType::PRIMITIVE)
  {
    result = result && print(out, vectorType::name[v_type], '<');
  }

  result = result && print(out, primitiveType::name[p_type]);

  if (v_type != vectorType::PRIMITIVE)
  {
    result = result && print(out, '>');
  }

  return result;
}

template <typename S>
bool printTypeName(S out, glslPreprocessor::Type *type)
{
  if (type->type == glslPreprocessor::typeType::STRUCT)
  {
    return print(out, type->sType->name);
  }
  else if (type->type == glslPreprocessor::typeType::VALUE)
  {
    return printValueTypeName(out, type->pType, type->vType);
  }
  else // type->type == glslPreprocessor::typeType::SAMPLER
  {
    return print(out, type->samplerDesc);
  }
}

template <typename S>
bool printArraySizes(S out, u32 array_dims, u64 *array_sizes)
{
  bool result = true;

  for (u32 i = 0; result && (i < array_dims); ++i)
  {
    u64 size = array_sizes[i];
    if (size) result &= print(out, '[', fmt(size), ']');
    else      result &= print(out, "[]"_s);
  }

  return result;
}

template <typename S>
bool print(S out, glslPreprocessor::ValueTypePrintCommand cmd)
{
  return printValueTypeName(out, cmd.pType, cmd.vType);
}

template <typename S>
bool print(S out, glslPreprocessor::TypeDeclarationPrintCommand cmd)
{
  return printTypeName(out, cmd.type) &&
         print(out, ' ', cmd.name) &&
         printArraySizes(out, cmd.type->arrayDims, cmd.type->arraySizes);
}

namespace glslPreprocessor
{
  namespace shaderStage
  {
    enum Enum
    {
      INVALID = -1,
      VERT,
      FRAG,
      ENUM_COUNT
    };

    bool isValid(Enum e)
    {
      return e == VERT ||
             e == FRAG;
    }

    Str suffix[] =
    {
      "vert"_s,
      "frag"_s
    };

    Str glEnumName[] =
    {
      "GL_VERTEX_SHADER"_s,
      "GL_FRAGMENT_SHADER"_s
    };

    EShLanguage glslangEnum[] =
    {
      EShLangVertex,
      EShLangFragment
    };

    Enum fromPath(Str path)
    {
      Enum result = INVALID;

      u64 last_dot_loc = path.findReverse('.');
      if (last_dot_loc != Str::NOT_FOUND)
      {
        Str this_suffix = path.trimLeft(last_dot_loc + 1);

        for (u32 i = 0; i < ENUM_COUNT; ++i)
        {
          if (this_suffix == suffix[i])
          {
            result = (Enum) i;
            break;
          }
        }
      }

      return result;
    }

    DEFINE_ENUM_PREINCREMENT
  }

  struct ShaderInput
  {
    Str   name;
    Type *type;
    u32   location;
  };

  struct InputFormat
  {
    Str name;
    u32 count;
    ShaderInput *inputs[0];

    static size_t sizeWith(u32 count)
    {
      return offsetOf(InputFormat, inputs[count]);
    }

    void setSize(u32 count)
    {
      this->count = count;
    }

    NO_COPY_OR_MOVE(InputFormat)
  };

  struct ShaderStorage
  {
    Str   name;
    Str   bindingName;
    Type *type;
  };

  struct ShaderFile
  {
    ZStr                 path;
    Str                  name;
    shaderStage::Enum    stage;
    File                *data;
    char                *source;
    glslang::TShader    *shader;
    bool                 compileSuccess;
    List<ShaderInput>    input;
    List<ShaderStorage>  uniforms;
    List<ShaderStorage>  buffers;

    ShaderFile(ZStr path):
      path(path)
    {}

    NO_COPY_OR_MOVE(ShaderFile)
  };

  struct ShaderProgram
  {
    Str                 name;
    ShaderFile         *files[shaderStage::ENUM_COUNT];
    bool                linkSuccess;
    InputFormat        *inputFormat;
    List<ShaderStorage> uniforms;
    List<ShaderStorage> buffers;

    ShaderProgram(Str name) :
      name(name)
    {}

    NO_COPY_OR_MOVE(ShaderProgram)
  };

  namespace msgType
  {
    enum Enum
    {
      NOTE,
      WARN,
      ERR,
      ENUM_COUNT
    };

    Str name[ENUM_COUNT] =
    {
      "Note"_s,
      "Warning"_s,
      "Error"_s
    };
  }

  struct Msg
  {
    msgType::Enum type;
    Str           text;
  };

  struct ShaderData
  {
    u32                 fileCount;
    List<ShaderFile>    files;
    List<ShaderProgram> programs;
    List<InputFormat>   inputFormats;
    List<Type>          types;
    List<StructType>    structs;
    List<Msg>           msgs;

    // List<UniformBlock>  blocks;

    ShaderData() :
      fileCount(0)
    {}

    NO_COPY_OR_MOVE(ShaderData)

    template <typename... Ts>
    void addMsg(PageAllocator *al,
                msgType::Enum   type,
                Ts...           args);

    ShaderFile *findOrAddFile(PageAllocator *al,
                              ZStr           path);

    ShaderProgram *addProgram(PageAllocator *al,
                              Str            name);

    void linkPrograms(PageAllocator *al);

    template <typename T>
    void printShadersCpp(PageAllocator al, T out);
  };


  inline namespace
  {
    namespace shaderStorageType
    {
      enum Enum
      {
        BUFFER,
        UNIFORM,
        ENUM_COUNT
      };
    }

    using namespace glslang;

    struct ShaderInputTraverser : TIntermTraverser
    {
      PageAllocator *al;
      ShaderData    *shaderData;
      ShaderFile    *shaderFile;
      TIntermediate *interm;

      ShaderInputTraverser(PageAllocator *al, ShaderData *shader_data,
                             ShaderFile *shader_file):
        al(al), shaderData(shader_data),
        shaderFile(shader_file)
      {
        // Thanks, Bjarne.
        struct Thief : TShader { TIntermediate *getInterm() { return intermediate; } };
        interm = ((Thief *)shaderFile->shader)->getInterm();
      }

      void go()
      {
        interm->getTreeRoot()->traverse(this);
      }

      int getOffset(TType *, size_t);
      int getSize(TType *);
      bool sameType(TType *, Type *);
      Type *findOrAddType(TType *);
      Type *findOrAddNonArrayType(StructType *);
      StructType *findOrAddStructType(TType *);

      // Shader AST:
      // └─┬ Aggregate: Sequence
      //   .
      //   .   functions etc.
      //   .
      //   └─┬ Aggregate: LinkerObjects
      //     ├── Symbol: e.g. Vertex array input
      //     ├── Symbol: e.g. Uniform
      //     ├── Symbol: e.g. Buffer
      //     └── Symbol: e.g. builtins etc.
      //
      // We only care about the linker objects, so abort traversal for
      // anything else

      virtual void visitSymbol(TIntermSymbol* node)
      {
        TStorageQualifier node_storage = node->getQualifier().storage;

        switch (node_storage)
        {
          case EvqVaryingIn:
          {
            if (shaderFile->stage == shaderStage::VERT)
            {
              visitVertexShaderInput(node);
            }
          } break;

          case EvqBuffer:
          {
            visitShaderStorage(node, shaderStorageType::BUFFER);
          } break;

          case EvqUniform:
          {
            visitShaderStorage(node, shaderStorageType::UNIFORM);
          } break;
        }
      }

      void visitVertexShaderInput(TIntermSymbol* node)
      {
        Str node_name = zStr(node->getName().c_str());
        TQualifier &node_qual = node->getQualifier();

        if (!node_qual.hasLocation())
        {
          shaderData->addMsg(al, msgType::ERR,
            "Vertex shader \""_s, shaderFile->name,
              "\" input \""_s, node_name, "\" has no location"_s
          );
          return;
        }

        Type *type = findOrAddType((TType *)&node->getType());

        // Vertex shader inputs can't be structs, samplers, or arrays of
        // undefined size. Shader file shouldn't have compiled.
        assert(type->type == typeType::VALUE && 0 < type->arrayTotalSize());

        auto *link = allocUninit<List<ShaderInput>::Link>(al);
        ShaderInput *input = &link->data;
        input->name = sprint(al, node_name)->str();
        input->type = type;
        input->location = node_qual.layoutLocation;
        shaderFile->input.append(link);
      }

      void visitShaderStorage(TIntermSymbol* node, shaderStorageType::Enum storage_type)
      {
        using namespace shaderStorageType;

        Str node_name = zStr(node->getName().c_str());

        if (node_name.find('@') != Str::NOT_FOUND)
        {
          shaderData->addMsg(al, msgType::ERR,
            "Illegal "_s,
              (storage_type == UNIFORM ? "uniform"_s : "buffer"_s),
              " identifier \""_s, node_name, "\" in file \""_s,
              shaderFile->name, "\""_s
          );
        }

        TType *node_type = (TType *)&node->getType();

        Type *type = findOrAddType(node_type);
        size_t array_total_size = type->arrayTotalSize();

        if (array_total_size == 0)
        {
          shaderData->addMsg(al, msgType::ERR,
            (storage_type == UNIFORM ? "Uniform"_s : "Buffer"_s),
              " \""_s, node_name, "\" in file \""_s, shaderFile->name,
              "\" is an array of undefined size"_s
          );

          for (u32 i = 0; i < type->arrayDims; ++i)
          {
            shaderData->addMsg(al, msgType::ERR, fmt(type->arraySizes[i]));
          }
        }

        TBasicType node_basic_type = node_type->getBasicType();

        if (node_basic_type == EbtStruct)
        {
          assert(storage_type == UNIFORM &&
                 type->type == typeType::STRUCT);

          // TODO:
          //
          // Recurse through structs and arrays of structs to output a single
          // uniform variable for each primitive variable or array, giving
          // each it's fully qualified name.

          shaderData->addMsg(al, msgType::NOTE,
            "Skipping uniform \""_s, zStr(node->getName().c_str()),
              "\" of struct \""_s, type->sType->name, "\" in file \""_s,
              shaderFile->name, "\" (struct uniforms not yet implemented)"_s
          );
        }
        else if (array_total_size == 1 || node_basic_type != EbtBlock)
        {
          typedef List<ShaderStorage>::Link Link;
          Link *link = allocUninit<Link>(al);
          ShaderStorage *storage = &link->data;
          storage->name = sprint(al, node_name)->str();
          storage->type = type;

          if (node_basic_type == EbtBlock)
          {
            storage->bindingName = type->sType->name;
          }
          else
          {
            assert(storage_type == UNIFORM);
            storage->bindingName = storage->name;
          }

          if (storage_type == BUFFER) shaderFile->buffers.append(link);
          else                           shaderFile->uniforms.append(link);
        }
        else
        {
          assert(node_basic_type == EbtBlock &&
                 array_total_size != 1 &&
                 type->type == typeType::STRUCT);

          if (1 < type->arrayDims)
          {
            shaderData->addMsg(al, msgType::ERR,
              "Multi-dimensional block arrays, such as "_s,
                (storage_type == UNIFORM ? "uniform"_s : "buffer"_s),
                " \""_s, node_name, "\" in file \""_s,
                shaderFile->name,
                "\" are forbidden (they crash my AMD driver!)"_s
            );
          }
          else
          {
            Type *non_array_type = findOrAddNonArrayType(type->sType);

            for (size_t i = 0; i < array_total_size; ++i)
            {
              auto *link = allocUninit<List<ShaderStorage>::Link>(al);
              ShaderStorage *storage = &link->data;
              storage->name = sprint(al, node_name, '_', fmt(i))->str();
              storage->bindingName = sprint(al, type->sType->name, '[', fmt(i), ']')->str();
              storage->type = non_array_type;

              if (storage_type == BUFFER) shaderFile->buffers.append(link);
              else                        shaderFile->uniforms.append(link);
            }
          }
        }
      }

      virtual bool visitAggregate(TVisit, TIntermAggregate* node)
      {
        TOperator op = node->getOp();
        return op == EOpSequence ||
               op == EOpLinkerObjects;
      }

      virtual bool visitBinary   (TVisit, TIntermBinary*)    { return false; }
      virtual bool visitUnary    (TVisit, TIntermUnary*)     { return false; }
      virtual bool visitSelection(TVisit, TIntermSelection*) { return false; }
      virtual bool visitLoop     (TVisit, TIntermLoop*)      { return false; }
      virtual bool visitBranch   (TVisit, TIntermBranch*)    { return false; }
      virtual bool visitSwitch   (TVisit, TIntermSwitch*)    { return false; }
    };

    bool ShaderInputTraverser::sameType(TType *theirs, Type *mine)
    {
      TArraySizes *their_arrayness = (TArraySizes *)((const TType *)theirs)->getArraySizes();
      if (their_arrayness)
      {
        u32 their_array_dims = castOrAssert<u32>(their_arrayness->getNumDims());
        if (their_array_dims != mine->arrayDims) return false;

        for (u32 i = 0; i < their_array_dims; ++i)
        {
          if (their_arrayness->getDimSize(i) != mine->arraySizes[i]) return false;
        }
      }
      else
      {
        if (mine->arrayDims != 0) return false;
      }

      TBasicType their_basic_type = theirs->getBasicType();

      if (their_basic_type == EbtBlock ||
          their_basic_type == EbtStruct)
      {
        if (!mine->type == typeType::STRUCT) return false;
        StructType *my_struct = mine->sType;

        Str their_name = zStr(theirs->getTypeName().c_str());

        if (their_name != my_struct->name) return false;

        TTypeList *their_struct = (TTypeList *)theirs->getStruct();
        assert(their_struct);
        size_t their_member_count = their_struct->size();

        if (their_member_count != my_struct->memberCount) return false;

        if (!my_struct->complete)
        {
          shaderData->addMsg(al, msgType::ERR,
            "Uh oh! Comparing shader struct type \""_s, their_name,
              "\" with incomplete C++ struct type!"_s
          );
        }
        else
        {
          for (size_t i = 0; i < their_member_count; ++i)
          {
            if (getOffset(theirs, i) != my_struct->members[i].offset) return false;

            if (zStr((*their_struct)[i].type->getFieldName().c_str()) !=
                  my_struct->members[i].name)
            {
              return false;
            }

            if (!sameType((*their_struct)[i].type, my_struct->members[i].type)) return false;
          }
        }
      }
      else
      {
        if (mine->type == typeType::STRUCT) return false;

        if (mine->pType != primitiveType::fromShaderType(theirs->getBasicType())) return false;

        if (theirs->isVector())
        {
          if (mine->vType != vectorType::v(theirs->getVectorSize())) return false;
        }
        else if (theirs->isMatrix())
        {
          if (mine->vType != vectorType::m(theirs->getMatrixCols(),
                                             theirs->getMatrixRows()))
          {
            return false;
          }
        }
        else
        {
          if (mine->vType != vectorType::PRIMITIVE) return false;
        }
      }

      return true;
    }

    Type *ShaderInputTraverser::findOrAddType(TType *type)
    {
      Type *result = 0;

      forEachLinkData (it, shaderData->types)
      {
        if (sameType(type, it))
        {
          result = it;
          break;
        }
      }

      if (!result)
      {
        u32 array_dims = 0;
        TArraySizes *arrayness = (TArraySizes *)((const TType *)type)->getArraySizes();
        if (arrayness) array_dims = castOrAssert<u32>(arrayness->getNumDims());

        auto *link = allocWith<List<Type>::Link>(al, array_dims);
        shaderData->types.append(link);
        result = &link->data;

        for (u32 i = 0; i < array_dims; ++i)
        {
          result->arraySizes[i] = arrayness->getDimSize(i);
        }

        TBasicType basic_type = type->getBasicType();

        if (basic_type == EbtSampler)
        {
          result->type = typeType::SAMPLER;
          result->samplerDesc = sprint(al, zStr(type->getSampler().getString().c_str()))->str();
        }
        else if (basic_type == EbtBlock ||
                 basic_type == EbtStruct)
        {
          result->type = typeType::STRUCT;
          result->sType = findOrAddStructType(type);

          if ((result->sType->sizeType() != structSizeType::FIXED) &&
              (array_dims != 0))
          {
            shaderData->addMsg(al, msgType::ERR,
              "Cannot specify array of variable size struct \""_s, result->sType->name, "\""_s
            );
          }
        }
        else
        {
          result->type  = typeType::VALUE;
          result->pType = primitiveType::fromShaderType(basic_type);

          if      (type->isVector()) result->vType = vectorType::v(type->getVectorSize());
          else if (type->isMatrix()) result->vType = vectorType::m(type->getMatrixCols(),
                                                                   type->getMatrixRows());
          else                       result->vType = vectorType::PRIMITIVE;
        }
      }

      return result;
    }

    Type *ShaderInputTraverser::findOrAddNonArrayType(StructType *type)
    {
      Type *result = 0;

      forEachLinkData (it, shaderData->types)
      {
        if (it->arrayDims == 0 &&
            it->type == typeType::STRUCT &&
            it->sType == type)
        {
          result = it;
          break;
        }
      }

      if (!result)
      {
        auto *link = allocWith<List<Type>::Link>(al, 0);
        shaderData->types.append(link);
        result = &link->data;

        result->type = typeType::STRUCT;
        result->sType = type;
      }

      return result;
    }

    StructType *ShaderInputTraverser::findOrAddStructType(TType *input_type)
    {
      // Input type might be an array so lets clone it and un-array it.
      TType type;
      type.shallowCopy(*input_type);
      type.clearArraySizes();

      StructType *result = 0;

      Str name = zStr(type.getTypeName().c_str());

      forEachLinkData (it, shaderData->structs)
      {
        if (it->name != name) continue;
        result = it;
      }

      TTypeList *structure = (TTypeList *)type.getStruct();
      assert(structure);
      size_t member_count = structure->size();

      if (!result)
      {
        typedef List<StructType>::Link Link;
        Link *link = allocWith<Link>(al, member_count);
        result = &link->data;
        result->complete = false;
        result->name = sprint(al, name)->str();
        result->size = getSize(&type);
        result->memberCount = member_count;
        shaderData->structs.append(link);

        size_t end_offset_of_prev_member = 0;

        for (size_t i = 0; i < member_count; ++i)
        {
          size_t offset = getOffset(&type, i);

          if (offset < end_offset_of_prev_member)
          {
            shaderData->addMsg(al, msgType::ERR,
              "Impossible struct layout. Member "_s, result->name, '.',
                result->members[i].name, " is at offset "_s,
                fmt(offset), " but previous field ends at "_s,
                fmt(end_offset_of_prev_member)
            );
          }

          result->members[i].offset = offset;
          result->members[i].name   = sprint(al, zStr((*structure)[i].type->getFieldName().c_str()))->str();
          result->members[i].type   = findOrAddType((*structure)[i].type);

          end_offset_of_prev_member = offset + result->members[i].type->size();

          if (i < member_count-1 && result->members[i].type->arrayTotalSize() == 0)
          {
            shaderData->addMsg(al, msgType::ERR,
              "Non final struct member "_s, result->name, '.',
                result->members[i].name, " has undefined array size"_s
            );
          }
        }
        result->complete = true;
      }
      else
      {
        bool check_struct_members = true;

        if (!result->complete)
        {
          check_struct_members = false;
          shaderData->addMsg(al, msgType::ERR,
            "Incomplete struct type \""_s, name,
              "\" found while building a struct. Circular reference?"_s
          );
        }

        if (member_count != result->memberCount)
        {
          check_struct_members = false;
          shaderData->addMsg(al, msgType::ERR,
            "Struct type \""_s, name, "\" has inconsistent member count ("_s,
              fmt(member_count), " / "_s, fmt(result->memberCount), "), struct name collision?"_s
          );
        }

        if (check_struct_members) for (size_t i = 0; i < member_count; ++i)
        {
          Str member_name = zStr((*structure)[i].type->getFieldName().c_str());
          if (member_name != result->members[i].name)
          {
            shaderData->addMsg(al, msgType::ERR,
            "Struct type \""_s, name, "\" has mismatched member "_s, fmt(i), " names (\""_s,
              member_name, "\" / \""_s, result->members[i].name, "\"), struct name collision?"_s
            );
          }

          if (!sameType((*structure)[i].type, result->members[i].type))
          {
            shaderData->addMsg(al, msgType::ERR,
            "Struct type \""_s, name, "\" has mismatched \""_s,
              member_name, "\" (member "_s, fmt(i), ") types, struct name collision?"_s
            );
          }
        }
      }

      return result;
    }

    // Copied from glslang > reflection.cpp > TLiveTraverser
    int ShaderInputTraverser::getOffset(TType *type, size_t index)
    {
      TTypeList *memberList = (TTypeList *)type->getStruct();

      // Don't calculate offset if one is present, it could be user supplied
      // and different than what would be calculated.  That is, this is faster,
      // but not just an optimization.
      if ((*memberList)[index].type->getQualifier().hasOffset())
          return (*memberList)[index].type->getQualifier().layoutOffset;

      int memberSize;
      int dummyStride;
      int offset = 0;
      for (int m = 0; m <= index; ++m) {
        // modify just the children's view of matrix layout, if there is one for this member
        TLayoutMatrix subMatrixLayout = (*memberList)[m].type->getQualifier().layoutMatrix;
        int memberAlignment = interm->getBaseAlignment(
          *(*memberList)[m].type,
          memberSize,
          dummyStride,
          type->getQualifier().layoutPacking == ElpStd140,
          subMatrixLayout != ElmNone ? subMatrixLayout == ElmRowMajor
                                     : type->getQualifier().layoutMatrix == ElmRowMajor);
        RoundToPow2(offset, memberAlignment);
        if (m < index) offset += memberSize;
      }

      return offset;
    }

    // Copied from glslang > reflection.cpp > TLiveTraverser
    int ShaderInputTraverser::getSize(TType *type)
    {
      int size;
      int stride;

      interm->getBaseAlignment(*type,
                               size,
                               stride,
                               type->getQualifier().layoutPacking == ElpStd140,
                               type->getQualifier().layoutMatrix == ElmRowMajor);

      return size;
    }
  }

  template <typename... Ts>
  void ShaderData::addMsg(PageAllocator *al,
                          msgType::Enum  type,
                          Ts...          args)
  {
    auto *link = alloc<List<Msg>::Link>(al);

    link->data.type = type;
    link->data.text = sprint(al, args...)->str();

    msgs.append(link);
  }

  ShaderFile *ShaderData::findOrAddFile(PageAllocator *al,
                                        ZStr           path)
  {
    ShaderFile *result = 0;

    BREAKABLE_START
    {
      forEachLinkData (file, files) if (path == file->path)
      {
        result = file;
        break;
      }

      if (result) break;

      shaderStage::Enum stage =
        shaderStage::fromPath(path);

      if (!shaderStage::isValid(stage)) break;

      {
        auto *link =
          alloc<List<ShaderFile>::Link>(al);
        result = &link->data;
        new (result) ShaderFile(path);
        files.append(link);
      }

      {
        Str file_name = path;
        Str::MultiFindResult last_slash =
          file_name.findReverse({ '\\', '/' });
        if (last_slash.location != Str::NOT_FOUND)
        {
          file_name = file_name.trimLeft(last_slash.location + 1);
        }

        StrBuf::Variable *buf = sprint(al, file_name, "_"_s, fmt(++fileCount));
        buf->replaceAll('.', '_');

        result->name = buf->str();
      }

      result->stage = stage;

      result->data = filesystem::load(STD_HEAP, result->path);

      if (!result->data)
      {
        addMsg(al, msgType::ERR,
               "Failed to load shader file: "_s, result->path);
        break;
      }

      result->data->mungeCRLF();
      result->source = (char *)result->data->data;

      {
        EShLanguage glslang_stage =
          shaderStage::glslangEnum[result->stage];
        result->shader = new glslang::TShader(glslang_stage);
      }

      result->shader->setStrings(&result->source, 1);

      result->compileSuccess =
        result->shader->parse(&glslang::DefaultTBuiltInResource,
                              110, false, EShMsgDefault);

      if (result->compileSuccess)
      {
        ShaderInputTraverser traverser(al, this, result);
        traverser.go();
      }
      else
      {
        addMsg(al, msgType::ERR,
               "Shader compilation failed: "_s, path);

        {
          Str log = zStr(result->shader->getInfoLog());
          if (log.length) addMsg(al, msgType::NOTE, "Info Log:\n"_s, log);
        }

        {
          Str log = zStr(result->shader->getInfoDebugLog());
          if (log.length) addMsg(al, msgType::NOTE, "Info Debug Log:\n"_s, log);
        }
      }
    }
    BREAKABLE_END

    return result;
  }

  ShaderProgram *ShaderData::addProgram(PageAllocator *al,
                                        Str            name)
  {
    ShaderProgram *result = 0;

    bool exists = false;
    forEachLinkData (program, programs) if (name == program->name)
    {
      exists = true;
      break;
    }

    if (!exists)
    {
      auto *link = alloc<List<ShaderProgram>::Link>(al);
      result = &link->data;
      new (result) ShaderProgram(name);
      programs.append(link);
    }

    return result;
  }

  void ShaderData::linkPrograms(PageAllocator *al)
  {
    forEachLinkData (program, programs)
    {
      TProgram linked_program;

      forEachEnum (stage, shaderStage)
      {
        ShaderFile *file = program->files[stage];
        if (file && file->compileSuccess) linked_program.addShader(file->shader);
      }

      program->linkSuccess = linked_program.link(EShMsgDefault);

      if (!program->linkSuccess)
      {
        addMsg(al, msgType::ERR, "Program link failed: "_s, program->name);

        {
          Str log = zStr(linked_program.getInfoLog());
          if (log.length) addMsg(al,
                                 msgType::NOTE,
                                 log);
        }

        {
          Str log = zStr(linked_program.getInfoDebugLog());
          if (log.length) addMsg(al,
                                 msgType::NOTE,
                                 log);
        }

        continue;
      }

      assert(program->files[shaderStage::VERT]);

      // Determine input format
      {
        // TODO: move this somewhere more obvious?
        constexpr u32 MAX_SHADER_INPUTS = 16;

        ShaderFile *vert_shader = program->files[shaderStage::VERT];

        ShaderInput *input_layout[MAX_SHADER_INPUTS] = {};
        u32 shader_input_count = 0;

        forEachLinkData (shader_input, vert_shader->input)
        {
          u32 loc = shader_input->location;

          if (input_layout[loc])
          {
            addMsg(al, msgType::ERR,
              "Duplicate attribute location "_s, fmt(loc),
                " in program \""_s, program->name, "\""_s
            );
            continue;
          }

          input_layout[loc] = shader_input;
          ++shader_input_count;
        }

        forEachLinkData (format, inputFormats)
        {
          if (format->count != shader_input_count) continue;

          bool ok = true;
          for (u32 i = 0; i < shader_input_count; ++i)
          {
            ShaderInput *format_input = format->inputs[i];
            ShaderInput *shader_input = input_layout[format_input->location];

            if (!shader_input ||
                shader_input->name     != format_input->name ||
                shader_input->type     != format_input->type ||
                shader_input->location != format_input->location)
            {
              ok = false;
              break;
            }
          }

          if (ok)
          {
            program->inputFormat = format;
            break;
          }
        }

        if (!program->inputFormat)
        {
          auto *link = allocUninitWith<List<InputFormat>::Link>(al, shader_input_count);
          InputFormat *format = program->inputFormat = &link->data;
          format->name = sprint(al, "Format"_s, fmt(inputFormats.count() + 1))->str();

          u32 input_idx = 0;
          for (u32 loc = 0; loc < arrayCount(input_layout); ++loc)
          {
            ShaderInput *input = input_layout[loc];
            if (!input) continue;

            format->inputs[input_idx++] = input;

            if (input->type->vType == vectorType::M4)
            {
              u32 input_location_span = 4;

              if (MAX_SHADER_INPUTS < loc + input_location_span)
              {
                addMsg(al, msgType::ERR,
                  "Invalid location "_s, fmt(loc), " for matrix input \""_s,
                    input->name, "\" in program \""_s, program->name,
                    "\" (NB. matrix inputs span multiple locations, and there are only "_s,
                    fmt(MAX_SHADER_INPUTS), "...)"_s
                );
              }

              for (u32 i = loc + 1;
                   i < min(loc + input_location_span, arrayCount(input_layout));
                   ++i)
              {
                if (input_layout[i])
                {
                  addMsg(al, msgType::ERR,
                    "Duplicate attribute location "_s, fmt(i),
                      " in program \""_s, program->name,
                      "\" (NB. matrix inputs span multiple locations)"_s
                  );
                }
              }
            }
            else if (input->type->vType != vectorType::PRIMITIVE &&
                     input->type->vType != vectorType::V2 &&
                     input->type->vType != vectorType::V3 &&
                     input->type->vType != vectorType::V4)
            {
              assert(!"FIXME: support other types");
            }
          }
          inputFormats.append(link);
        }
      }

      forEachEnum (stage, shaderStage)
      {
        ShaderFile *file = program->files[stage];
        if (!file) continue;

        forEachLinkData (file_uniform, file->uniforms)
        {
          bool already_added = false;

          forEachLinkData (program_uniform, program->uniforms)
          {
            if (program_uniform->name == file_uniform->name)
            {
              if (program_uniform->type != file_uniform->type)
              {
                addMsg(al, msgType::ERR,
                  "Overlapped uniform name \""_s, program_uniform->name,
                    "\" with differing types in program \""_s, program->name,
                    "\""_s
                );
              }

              already_added = true;
              break;
            }
          }

          if (!already_added)
          {
            auto *link = alloc<List<ShaderStorage>::Link>(al);
            link->data = *file_uniform;
            program->uniforms.append(link);
          }
        }

        forEachLinkData (file_buffer, file->buffers)
        {
          bool already_added = false;

          forEachLinkData (program_buffer, program->buffers)
          {
            if (program_buffer->name == file_buffer->name)
            {
              if (program_buffer->type != file_buffer->type)
              {
                addMsg(al, msgType::ERR,
                  "Overlapped buffer name \""_s, program_buffer->name,
                    "\" with differing types in program \""_s, program->name,
                    "\""_s
                );
              }

              already_added = true;
              break;
            }
          }

          if (!already_added)
          {
            auto *link = alloc<List<ShaderStorage>::Link>(al);
            link->data = *file_buffer;
            program->buffers.append(link);
          }
        }
      }
    }
  }


  template <typename T>
  void ShaderData::printShadersCpp(PageAllocator al, T out)
  {
    print(out,
      "namespace shaderAssets\n"
      "{\n"
      "  using namespace shaders::glsl;\n"
      "\n"
      "  gl3w::Functions *gl;"_s
    );

    /////////////////////////
    // Print input formats //
    /////////////////////////

    if (!inputFormats.isEmpty())
    {
      print(out, "\n"
        "\n"
        "  namespace inputFormats\n"
        "  {"_s
      );

      forEachLinkData (format, inputFormats)
      {
        print(out, "\n"
          "    struct "_s, format->name, "\n"
          "    {"_s
        );

        for (u32 i = 0; i < format->count; ++i)
        {
          ShaderInput *input = format->inputs[i];

          assert(0 < input->name.length);
          char name_capital_first_char = toupper(input->name.data[0]);
          Str name_minus_first_char = input->name.trimLeft(1);

          if (input->type->vType == vectorType::PRIMITIVE ||
              input->type->vType == vectorType::V2 ||
              input->type->vType == vectorType::V3 ||
              input->type->vType == vectorType::V4)
          {
            print(out, "\n"
              "      template <typename InputVertex>\n"
              "      static "_s,
                printValueType(input->type->pType, input->type->vType),
                " *addressOf"_s, name_capital_first_char, name_minus_first_char,
                "(InputVertex *v);\n"
              "\n"
              "      template <typename InputVertex>\n"
              "      static void configure"_s, name_capital_first_char,
                name_minus_first_char, "Attribute(gl3w::Functions *gl, GLuint divisor)\n"
              "      {\n"
              "        gl->EnableVertexAttribArray("_s, fmt(input->location), ");\n"
              "        gl->VertexAttribPointer("_s, fmt(input->location), ", "_s,
                fmt(vectorType::count(input->type->vType)), ", "_s,
                primitiveType::glEnumName(input->type->pType),
                ", GL_FALSE, sizeof(InputVertex), (void *) addressOf"_s,
                name_capital_first_char, name_minus_first_char, "<InputVertex>(0));\n"
              "        gl->VertexAttribDivisor("_s, fmt(input->location), ", divisor);\n"
              "      }\n"_s
            );
          }
          else if (input->type->vType == vectorType::M4)
          {
            print(out, "\n"
              "      template <typename InputVertex>\n"
              "      static "_s,
                printValueType(input->type->pType, input->type->vType),
                " *addressOf"_s, name_capital_first_char, name_minus_first_char,
                "(InputVertex *v);\n"
              "\n"
              "      template <typename InputVertex>\n"
              "      static void configure"_s, name_capital_first_char,
                name_minus_first_char, "Attribute(gl3w::Functions *gl, GLuint divisor)\n"
              "      {\n"
              "        "_s, printValueType(input->type->pType, input->type->vType),
                " *m = addressOf"_s, name_capital_first_char,
                name_minus_first_char, "<InputVertex>(0);\n"
              "\n"
              "        gl->EnableVertexAttribArray("_s, fmt(input->location + 0), ");\n"
              "        gl->VertexAttribPointer("_s, fmt(input->location + 0), ", 4, "_s,
                primitiveType::glEnumName(input->type->pType),
                ", GL_FALSE, sizeof(InputVertex), (void *) &m->cols[0]);\n"
              "        gl->VertexAttribDivisor("_s, fmt(input->location + 0), ", divisor);\n"
              "\n"
              "        gl->EnableVertexAttribArray("_s, fmt(input->location + 1), ");\n"
              "        gl->VertexAttribPointer("_s, fmt(input->location + 1), ", 4, "_s,
                primitiveType::glEnumName(input->type->pType),
                ", GL_FALSE, sizeof(InputVertex), (void *) &m->cols[1]);\n"
              "        gl->VertexAttribDivisor("_s, fmt(input->location + 1), ", divisor);\n"
              "\n"
              "        gl->EnableVertexAttribArray("_s, fmt(input->location + 2), ");\n"
              "        gl->VertexAttribPointer("_s, fmt(input->location + 2), ", 4, "_s,
                primitiveType::glEnumName(input->type->pType),
                ", GL_FALSE, sizeof(InputVertex), (void *) &m->cols[2]);\n"
              "        gl->VertexAttribDivisor("_s, fmt(input->location + 2), ", divisor);\n"
              "\n"
              "        gl->EnableVertexAttribArray("_s, fmt(input->location + 3), ");\n"
              "        gl->VertexAttribPointer("_s, fmt(input->location + 3), ", 4, "_s,
                primitiveType::glEnumName(input->type->pType),
                ", GL_FALSE, sizeof(InputVertex), (void *) &m->cols[3]);\n"
              "        gl->VertexAttribDivisor("_s, fmt(input->location + 3), ", divisor);\n"
              "      }\n"_s
            );
          }
          else
          {
            assert(!"FIXME: support other types");
          }
        }

        print(out, "\n"
          "      template <typename InputVertex>\n"
          "      static void configureAllAttributes(gl3w::Functions *gl)\n"
          "      {"_s
        );

        for (u32 i = 0; i < format->count; ++i)
        {
          ShaderInput *input = format->inputs[i];

          // TODO: try to avoid doing this twice?
          assert(0 < input->name.length);
          char name_capital_first_char = toupper(input->name.data[0]);
          Str name_minus_first_char = input->name.trimLeft(1);

          print(out, "\n"
            "        configure"_s, name_capital_first_char,
              name_minus_first_char, "Attribute<InputVertex>(gl, 0);"_s
          );
        }

        print(out, "\n"
          "      }\n"
          "    };\n"_s
        );
      }

      print(out,
        "  }"_s
      );
    }

    ///////////////////
    // Print structs //
    ///////////////////

    if (!structs.isEmpty())
    {
      print(out, "\n"
        "\n"
        "  namespace structs\n"
        "  {\n"
        "    #pragma pack(push, 1)"_s
      );

      {
        PageAllocator struct_listing_al = al;

        List<StructType *> to_print;
        List<StructType *> printed;

        forEachLinkData (it, structs)
        {
          auto *link = allocUninit<List<StructType *>::Link>(&struct_listing_al);
          link->data = it;
          to_print.append(link);
        }

        while (true)
        {
          List<StructType *>::Link *link = 0;

          forEachLink (to_print_link, to_print)
          {
            StructType *structure = to_print_link->data;
            bool structure_is_ready_to_print = true;

            for (u64 i = 0; i < structure->memberCount; ++i)
            {
              if (!structure->members[i].type->type == typeType::STRUCT) continue;

              StructType *nested_struct =
                structure->members[i].type->sType;
              bool nested_struct_has_been_printed = false;

              forEachLink (printed_link, printed)
              {
                StructType *printed_struct = printed_link->data;
                if (printed_struct == nested_struct)
                {
                  nested_struct_has_been_printed = true;
                  break;
                }
              }

              if (!nested_struct_has_been_printed)
              {
                structure_is_ready_to_print = false;
                break;
              }
            }

            if (structure_is_ready_to_print) link = to_print_link;
          }

          if (!link)
          {
            if (to_print.isEmpty())
            {
              break;
            }
            else
            {
              // We must have a dependency cycle or a missing struct type.
              // Print the next thing anyway and let the compiler complain.
              link = to_print.first();
            }
          }

          link->remove();
          StructType *structure = link->data;

          print(out, "\n"
            "\n"
            "    struct "_s, structure->name, "\n"
            "    {"_s
          );

          size_t end_offset_of_prev_member = 0;
          u32 pad_count = 0;

          for (u64 i = 0; i < structure->memberCount; ++i)
          {
            if (end_offset_of_prev_member < structure->members[i].offset)
            {
              size_t padding = structure->members[i].offset - end_offset_of_prev_member;

              print(out, "\n"
                "      u8 pad"_s, fmt(pad_count++), "["_s, fmt(padding), "];"_s
              );
            }

            end_offset_of_prev_member = structure->members[i].offset + structure->members[i].type->size();

            print(out, "\n"
              "      "_s, printTypeDeclaration(structure->members[i].type,
                                          structure->members[i].name),
                ";"_s
            );
          }

          if (structure->size < end_offset_of_prev_member)
          {
            print(out, "\n"
              "      #error size mismatch "_s, fmt(structure->size), " vs "_s, fmt(end_offset_of_prev_member)
            );
          }
          else if (end_offset_of_prev_member < structure->size)
          {
            size_t padding = structure->size - end_offset_of_prev_member;

            print(out, "\n"
              "      u8 pad"_s, fmt(pad_count++), "["_s, fmt(padding), "];"_s
            );
          }

          structSizeType::Enum size_type = structure->sizeType();

          if (size_type != structSizeType::FIXED)
          {
            StructType::Member *final_member = &structure->members[structure->memberCount - 1];

            if (size_type == structSizeType::ARRAY)
            {
              print(out, "\n"
                "\n"
                "      static size_t sizeWith(size_t count)\n"
                "      {\n"
                "        return offsetOf("_s, structure->name,
                  ", "_s, final_member->name, "[count]);\n"
                "      }"_s
              );
            }
            else // size_type == structSizeType::STRUCT
            {
              assert(final_member->type->type == typeType::STRUCT);

              print(out, "\n"
                "\n"
                "      static size_t sizeWith(size_t count)\n"
                "      {\n"
                "        return offsetOf("_s, structure->name,
                  ", "_s, final_member->name, ") + "_s,
                  final_member->type->sType->name, "::sizeWith(count);\n"
                "      }"_s
              );
            }

            print(out, "\n"
              "\n"
              "      NO_COPY_OR_MOVE("_s, structure->name, ")"_s
            );
          }

          print(out, "\n"
            "    };"_s
          );
          printed.append(link);
        }
      }

      print(out, "\n"
        "\n"
        "    #pragma pack(pop)\n"
        "  }"_s
      );
    }

    print(out, "\n"
      "\n"
      "  namespace programs\n"
      "  {"_s
    );


    ////////////////////
    // Print programs //
    ////////////////////

    u32 uniform_blocks_count = 0;
    u32 storage_blocks_count = 0;

    forEachLinkData (program, programs)
    {
      print(out, "\n"
        "    namespace "_s, program->name, "\n"
        "    {\n"
        "      GLuint program;\n"
        "\n"
        "      typedef inputFormats::"_s, program->inputFormat->name, " InputFormat;"_s
      );

      forEachLinkData (uniform, program->uniforms)
      {
        size_t array_total_size = uniform->type->arrayTotalSize();

        // array of undefined size is not legal in uniform.
        assert(0 < array_total_size);

        print(out, "\n"
          "\n"
          "      // uniform "_s, printTypeDeclaration(uniform->type, uniform->name), "\n"
          "      namespace "_s, uniform->name, "\n"
          "      {"_s
        );

        if (uniform->type->type == typeType::STRUCT)
        {
          print(out, "\n"
            "        constexpr GLuint IDX = "_s, fmt(++uniform_blocks_count), ";\n"
            "\n"
            "        GLuint buf;\n"_s
          );

          if (array_total_size == 1)
          {
            print(out, "\n"
              "        void set(structs::"_s, uniform->type->sType->name,
                " *data, bufferUsage::Enum usage = bufferUsage::DYNAMIC_DRAW)\n"
              "        {""\n"
              "          uploadBuffer(gl, GL_UNIFORM_BUFFER, buf, sizeof(structs::"_s,
                uniform->type->sType->name, "), data, usage);"_s
            );
          }
          else // 1 < array_total_size
          {
            print(out, "\n"
              "        void set(structs::"_s, uniform->type->sType->name,
                " (*data)["_s, fmt(array_total_size), "], bufferUsage::Enum usage = bufferUsage::DYNAMIC_DRAW)\n"
              "        {""\n"
              "          uploadBuffer(gl, GL_UNIFORM_BUFFER, buf, "_s,
                fmt(array_total_size), " * sizeof(structs::"_s, uniform->type->sType->name,
                "), (*data), usage);"_s
            );
          }

          print(out, "\n"
            "        }"_s
          );
        }
        else
        {
          print(out, "\n"
            "        GLuint offset;"_s
          );

          primitiveType::Enum p_type;
          vectorType::Enum v_type;

          if (uniform->type->type == typeType::SAMPLER)
          {
            p_type = primitiveType::I32;
            v_type = vectorType::PRIMITIVE;
          }
          else
          {
            p_type = uniform->type->pType;
            v_type = uniform->type->vType;
          }

          if (array_total_size == 1)
          {
            if (v_type == vectorType::PRIMITIVE)
            {
              print(out, "\n"
                "\n"
                "        void set("_s, printValueType(p_type, v_type), " data)\n"
                "        {\n"
                "          gl->Uniform1"_s, primitiveType::glUniformTypeSuffix(p_type), "(offset, data);\n"
                "        }"_s
              );
            }
            else if (vectorType::isVector(uniform->type->vType))
            {
              print(out, "\n"
                "\n"
                "        void set("_s, printValueType(p_type, v_type), " data)\n"
                "        {\n"
                "          gl->Uniform"_s, fmt(vectorType::count(uniform->type->vType)),
                  primitiveType::glUniformTypeSuffix(p_type), "v(offset, "_s,
                  fmt(array_total_size), ", &data.v[0]);\n"
                "        }"_s
              );
            }
            else
            {
              assert(vectorType::isMatrix(uniform->type->vType));

              // don't support non-square matrices yet
              assert(vectorType::isSquare(uniform->type->vType));

              print(out, "\n"
                "\n"
                "        void set("_s, printValueType(p_type, v_type), " data)\n"
                "        {\n"
                "          gl->Uniform"_s, "Matrix"_s, fmt(vectorType::cols[uniform->type->vType]),
                  primitiveType::glUniformTypeSuffix(uniform->type->pType),
                  "v(offset, "_s, fmt(array_total_size), ", GL_FALSE, &data.v[0]);\n"
                "        }"_s
              );
            }
          }
          else if (v_type == vectorType::PRIMITIVE)
          {
            assert(1 < array_total_size);

            print(out, "\n"
              "\n"
              "        void set("_s, printValueType(p_type, v_type), " (*data)["_s, fmt(array_total_size), "])\n"
              "        {\n"
              "          gl->Uniform1"_s, primitiveType::glUniformTypeSuffix(p_type), "v(offset, "_s, fmt(array_total_size), ", data);\n"
              "        }"_s
            );
          }
          else if (vectorType::isVector(uniform->type->vType))
          {
            assert(1 < array_total_size);

            print(out, "\n"
              "\n"
              "        void set("_s, printValueType(p_type, v_type), " (*data)["_s, fmt(array_total_size), "])\n"
              "        {\n"
              "          gl->Uniform"_s, fmt(vectorType::count(uniform->type->vType)),
                primitiveType::glUniformTypeSuffix(p_type), "v(offset, "_s,
                fmt(array_total_size), ", &(*data)[0].v[0]);\n"
              "        }"_s
            );
          }
          else
          {
            assert(vectorType::isMatrix(uniform->type->vType));
            assert(1 < array_total_size);

            // don't support non-square matrices yet
            assert(vectorType::isSquare(uniform->type->vType));

            print(out, "\n"
              "\n"
              "        void set("_s, printValueType(p_type, v_type), " (*data)["_s, fmt(array_total_size), "])\n"
              "        {\n"
              "          gl->Uniform"_s, "Matrix"_s, fmt(vectorType::cols[uniform->type->vType]),
                primitiveType::glUniformTypeSuffix(uniform->type->pType),
                "v(offset, "_s, fmt(array_total_size), ", GL_FALSE, &(*data)[0].v[0]);\n"
              "        }"_s
            );
          }
        }

        print(out, "\n"
          "      }"_s
        );
      }

      forEachLinkData (buffer, program->buffers)
      {
        size_t array_total_size = buffer->type->arrayTotalSize();

        bool variable_size_struct = (buffer->type->type == typeType::STRUCT) &&
                                    (buffer->type->sType->sizeType() != structSizeType::FIXED);

        // Can't have an array of variable sized structs
        assert((array_total_size == 1) || !variable_size_struct);

        print(out, "\n"
          "\n"
          "      // buffer "_s, printTypeDeclaration(buffer->type, buffer->name), "\n"
          "      namespace "_s, buffer->name, "\n"
          "      {"_s
        );

        print(out, "\n"
          "        constexpr GLuint IDX = "_s, fmt(++storage_blocks_count), ";\n"
          "\n"
          "        GLuint buf;"_s
        );

        if (variable_size_struct || (array_total_size == 0))
        {
          print(out, "\n"
            "\n"
            "        void set(structs::"_s, buffer->type->sType->name,
              " *data, size_t count, bufferUsage::Enum usage = bufferUsage::DYNAMIC_DRAW)\n"
            "        {"_s
          );
        }
        else if (1 < array_total_size)
        {
          print(out, "\n"
            "\n"
            "        void set(structs::"_s, buffer->type->sType->name,
              " (*data)["_s, fmt(array_total_size), "], bufferUsage::Enum usage = bufferUsage::DYNAMIC_DRAW)\n"
            "        {"_s
          );
        }
        else // array_total_size == 1
        {
          print(out, "\n"
            "\n"
            "        void set(structs::"_s, buffer->type->sType->name,
              " *data, bufferUsage::Enum usage = bufferUsage::DYNAMIC_DRAW)\n"
            "        {"_s
          );
        }

        if (!variable_size_struct)
        {
          if (array_total_size == 1)
          {
            print(out, "\n"
              "          uploadBuffer(gl, GL_SHADER_STORAGE_BUFFER, buf, sizeof(structs::"_s,
                buffer->type->sType->name, "), data, usage);"_s
            );
          }
          else if (1 < array_total_size)
          {
            print(out, "\n"
              "          uploadBuffer(gl, GL_SHADER_STORAGE_BUFFER, buf, "_s,
                fmt(array_total_size), " * sizeof(structs::"_s, buffer->type->sType->name,
                "), (*data), usage);"_s
            );
          }
          else // array_total_size == 0
          {
            print(out, "\n"
              "          uploadBuffer(gl, GL_SHADER_STORAGE_BUFFER, buf, count * sizeof(structs::"_s,
                buffer->type->sType->name, "), data, usage);"_s
            );
          }
        }
        else // variable_size_struct
        {
          print(out, "\n"
            "          uploadBuffer(gl, GL_SHADER_STORAGE_BUFFER, buf, structs::"_s,
              buffer->type->sType->name, "::sizeWith(count), data, usage);"_s
          );
        }

        print(out, "\n"
          "        }\n"
          "      }"_s
        );
      }

      print(out, "\n"
        "\n"
        "      template <typename V>\n"
        "      void render(VertexArrayConfiguration<InputFormat, V> *cfg)\n"
        "      {\n"
        "        cfg->_render(gl);\n"
        "      }\n"
        "\n"
        "      template <typename V, typename I>\n"
        "      void render(InstancedVertexArrayConfiguration<InputFormat, V, I> *cfg, GLsizei count, I *instances)\n"
        "      {\n"
        "        cfg->_render(gl, count, instances);\n"
        "      }\n"
        "\n"
        "      template <u32 MESH_COUNT, typename V, typename I>\n"
        "      void render(MultiMeshVertexArrayConfiguration<InputFormat, V, I, MESH_COUNT> *cfg, PageAllocator tmp_alloc, GLsizei per_mesh_counts[MESH_COUNT], I *instances)\n"
        "      {\n"
        "        cfg->_render(gl, tmp_alloc, per_mesh_counts, instances);\n"
        "      }\n"
        "    }\n"_s
      );
    }

    ///////////////////////////
    // Print "init" function //
    ///////////////////////////

    print(out,
      "  }\n"
      "\n"
      "  Str getUniformTypeName(GLenum type)\n"
      "  {\n"
      "    switch (type)\n"
      "    {\n"
      "      case GL_FLOAT:                                     return \"float\"_s;\n"
      "      case GL_FLOAT_VEC2:                                return \"vec2\"_s;\n"
      "      case GL_FLOAT_VEC3:                                return \"vec3\"_s;\n"
      "      case GL_FLOAT_VEC4:                                return \"vec4\"_s;\n"
      "      case GL_DOUBLE:                                    return \"double\"_s;\n"
      "      case GL_DOUBLE_VEC2:                               return \"dvec2\"_s;\n"
      "      case GL_DOUBLE_VEC3:                               return \"dvec3\"_s;\n"
      "      case GL_DOUBLE_VEC4:                               return \"dvec4\"_s;\n"
      "      case GL_INT:                                       return \"int\"_s;\n"
      "      case GL_INT_VEC2:                                  return \"ivec2\"_s;\n"
      "      case GL_INT_VEC3:                                  return \"ivec3\"_s;\n"
      "      case GL_INT_VEC4:                                  return \"ivec4\"_s;\n"
      "      case GL_UNSIGNED_INT:                              return \"unsigned int\"_s;\n"
      "      case GL_UNSIGNED_INT_VEC2:                         return \"uvec2\"_s;\n"
      "      case GL_UNSIGNED_INT_VEC3:                         return \"uvec3\"_s;\n"
      "      case GL_UNSIGNED_INT_VEC4:                         return \"uvec4\"_s;\n"
      "      case GL_BOOL:                                      return \"bool\"_s;\n"
      "      case GL_BOOL_VEC2:                                 return \"bvec2\"_s;\n"
      "      case GL_BOOL_VEC3:                                 return \"bvec3\"_s;\n"
      "      case GL_BOOL_VEC4:                                 return \"bvec4\"_s;\n"
      "      case GL_FLOAT_MAT2:                                return \"mat2\"_s;\n"
      "      case GL_FLOAT_MAT3:                                return \"mat3\"_s;\n"
      "      case GL_FLOAT_MAT4:                                return \"mat4\"_s;\n"
      "      case GL_FLOAT_MAT2x3:                              return \"mat2x3\"_s;\n"
      "      case GL_FLOAT_MAT2x4:                              return \"mat2x4\"_s;\n"
      "      case GL_FLOAT_MAT3x2:                              return \"mat3x2\"_s;\n"
      "      case GL_FLOAT_MAT3x4:                              return \"mat3x4\"_s;\n"
      "      case GL_FLOAT_MAT4x2:                              return \"mat4x2\"_s;\n"
      "      case GL_FLOAT_MAT4x3:                              return \"mat4x3\"_s;\n"
      "      case GL_DOUBLE_MAT2:                               return \"dmat2\"_s;\n"
      "      case GL_DOUBLE_MAT3:                               return \"dmat3\"_s;\n"
      "      case GL_DOUBLE_MAT4:                               return \"dmat4\"_s;\n"
      "      case GL_DOUBLE_MAT2x3:                             return \"dmat2x3\"_s;\n"
      "      case GL_DOUBLE_MAT2x4:                             return \"dmat2x4\"_s;\n"
      "      case GL_DOUBLE_MAT3x2:                             return \"dmat3x2\"_s;\n"
      "      case GL_DOUBLE_MAT3x4:                             return \"dmat3x4\"_s;\n"
      "      case GL_DOUBLE_MAT4x2:                             return \"dmat4x2\"_s;\n"
      "      case GL_DOUBLE_MAT4x3:                             return \"dmat4x3\"_s;\n"
      "      case GL_SAMPLER_1D:                                return \"sampler1D\"_s;\n"
      "      case GL_SAMPLER_2D:                                return \"sampler2D\"_s;\n"
      "      case GL_SAMPLER_3D:                                return \"sampler3D\"_s;\n"
      "      case GL_SAMPLER_CUBE:                              return \"samplerCube\"_s;\n"
      "      case GL_SAMPLER_1D_SHADOW:                         return \"sampler1DShadow\"_s;\n"
      "      case GL_SAMPLER_2D_SHADOW:                         return \"sampler2DShadow\"_s;\n"
      "      case GL_SAMPLER_1D_ARRAY:                          return \"sampler1DArray\"_s;\n"
      "      case GL_SAMPLER_2D_ARRAY:                          return \"sampler2DArray\"_s;\n"
      "      case GL_SAMPLER_1D_ARRAY_SHADOW:                   return \"sampler1DArrayShadow\"_s;\n"
      "      case GL_SAMPLER_2D_ARRAY_SHADOW:                   return \"sampler2DArrayShadow\"_s;\n"
      "      case GL_SAMPLER_2D_MULTISAMPLE:                    return \"sampler2DMS\"_s;\n"
      "      case GL_SAMPLER_2D_MULTISAMPLE_ARRAY:              return \"sampler2DMSArray\"_s;\n"
      "      case GL_SAMPLER_CUBE_SHADOW:                       return \"samplerCubeShadow\"_s;\n"
      "      case GL_SAMPLER_BUFFER:                            return \"samplerBuffer\"_s;\n"
      "      case GL_SAMPLER_2D_RECT:                           return \"sampler2DRect\"_s;\n"
      "      case GL_SAMPLER_2D_RECT_SHADOW:                    return \"sampler2DRectShadow\"_s;\n"
      "      case GL_INT_SAMPLER_1D:                            return \"isampler1D\"_s;\n"
      "      case GL_INT_SAMPLER_2D:                            return \"isampler2D\"_s;\n"
      "      case GL_INT_SAMPLER_3D:                            return \"isampler3D\"_s;\n"
      "      case GL_INT_SAMPLER_CUBE:                          return \"isamplerCube\"_s;\n"
      "      case GL_INT_SAMPLER_1D_ARRAY:                      return \"isampler1DArray\"_s;\n"
      "      case GL_INT_SAMPLER_2D_ARRAY:                      return \"isampler2DArray\"_s;\n"
      "      case GL_INT_SAMPLER_2D_MULTISAMPLE:                return \"isampler2DMS\"_s;\n"
      "      case GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY:          return \"isampler2DMSArray\"_s;\n"
      "      case GL_INT_SAMPLER_BUFFER:                        return \"isamplerBuffer\"_s;\n"
      "      case GL_INT_SAMPLER_2D_RECT:                       return \"isampler2DRect\"_s;\n"
      "      case GL_UNSIGNED_INT_SAMPLER_1D:                   return \"usampler1D\"_s;\n"
      "      case GL_UNSIGNED_INT_SAMPLER_2D:                   return \"usampler2D\"_s;\n"
      "      case GL_UNSIGNED_INT_SAMPLER_3D:                   return \"usampler3D\"_s;\n"
      "      case GL_UNSIGNED_INT_SAMPLER_CUBE:                 return \"usamplerCube\"_s;\n"
      "      case GL_UNSIGNED_INT_SAMPLER_1D_ARRAY:             return \"usampler2DArray\"_s;\n"
      "      case GL_UNSIGNED_INT_SAMPLER_2D_ARRAY:             return \"usampler2DArray\"_s;\n"
      "      case GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE:       return \"usampler2DMS\"_s;\n"
      "      case GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY: return \"usampler2DMSArray\"_s;\n"
      "      case GL_UNSIGNED_INT_SAMPLER_BUFFER:               return \"usamplerBuffer\"_s;\n"
      "      case GL_UNSIGNED_INT_SAMPLER_2D_RECT:              return \"usampler2DRect\"_s;\n"
      "      case GL_IMAGE_1D:                                  return \"image1D\"_s;\n"
      "      case GL_IMAGE_2D:                                  return \"image2D\"_s;\n"
      "      case GL_IMAGE_3D:                                  return \"image3D\"_s;\n"
      "      case GL_IMAGE_2D_RECT:                             return \"image2DRect\"_s;\n"
      "      case GL_IMAGE_CUBE:                                return \"imageCube\"_s;\n"
      "      case GL_IMAGE_BUFFER:                              return \"imageBuffer\"_s;\n"
      "      case GL_IMAGE_1D_ARRAY:                            return \"image1DArray\"_s;\n"
      "      case GL_IMAGE_2D_ARRAY:                            return \"image2DArray\"_s;\n"
      "      case GL_IMAGE_2D_MULTISAMPLE:                      return \"image2DMS\"_s;\n"
      "      case GL_IMAGE_2D_MULTISAMPLE_ARRAY:                return \"image2DMSArray\"_s;\n"
      "      case GL_INT_IMAGE_1D:                              return \"iimage1D\"_s;\n"
      "      case GL_INT_IMAGE_2D:                              return \"iimage2D\"_s;\n"
      "      case GL_INT_IMAGE_3D:                              return \"iimage3D\"_s;\n"
      "      case GL_INT_IMAGE_2D_RECT:                         return \"iimage2DRect\"_s;\n"
      "      case GL_INT_IMAGE_CUBE:                            return \"iimageCube\"_s;\n"
      "      case GL_INT_IMAGE_BUFFER:                          return \"iimageBuffer\"_s;\n"
      "      case GL_INT_IMAGE_1D_ARRAY:                        return \"iimage1DArray\"_s;\n"
      "      case GL_INT_IMAGE_2D_ARRAY:                        return \"iimage2DArray\"_s;\n"
      "      case GL_INT_IMAGE_2D_MULTISAMPLE:                  return \"iimage2DMS\"_s;\n"
      "      case GL_INT_IMAGE_2D_MULTISAMPLE_ARRAY:            return \"iimage2DMSArray\"_s;\n"
      "      case GL_UNSIGNED_INT_IMAGE_1D:                     return \"uimage1D\"_s;\n"
      "      case GL_UNSIGNED_INT_IMAGE_2D:                     return \"uimage2D\"_s;\n"
      "      case GL_UNSIGNED_INT_IMAGE_3D:                     return \"uimage3D\"_s;\n"
      "      case GL_UNSIGNED_INT_IMAGE_2D_RECT:                return \"uimage2DRect\"_s;\n"
      "      case GL_UNSIGNED_INT_IMAGE_CUBE:                   return \"uimageCube\"_s;\n"
      "      case GL_UNSIGNED_INT_IMAGE_BUFFER:                 return \"uimageBuffer\"_s;\n"
      "      case GL_UNSIGNED_INT_IMAGE_1D_ARRAY:               return \"uimage1DArray\"_s;\n"
      "      case GL_UNSIGNED_INT_IMAGE_2D_ARRAY:               return \"uimage2DArray\"_s;\n"
      "      case GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE:         return \"uimage2DMS\"_s;\n"
      "      case GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_ARRAY:   return \"uimage2DMSArray\"_s;\n"
      "      case GL_UNSIGNED_INT_ATOMIC_COUNTER:               return \"atomic_uint\"_s;\n"
      "      default:                                           return \"UNKNOWN\"_s;\n"
      "    }\n"
      "  }\n"
      "\n"
      "  void printProgramResourceName(GLuint program, GLenum interface, GLint index)\n"
      "  {\n"
      "    GLenum name_length_prop = GL_NAME_LENGTH;\n"
      "    GLint name_length;\n"
      "    gl->GetProgramResourceiv(program, interface, index, 1, &name_length_prop, 1, 0, &name_length);\n"
      "\n"
      "    StrBuf::Variable *name = (StrBuf::Variable *) _alloca(StrBuf::Variable::sizeWith(name_length));\n"
      "    name->capacity = name_length;\n"
      "    name->length = name_length-1;\n"
      "    gl->GetProgramResourceName(program, interface, index, name_length, 0, name->data);\n"
      "\n"
      "    print(stderr, name->str());\n"
      "  }\n"
      "\n"
      "  void printProgramUniformDeclaration(GLuint program, GLint index)\n"
      "  {\n"
      "    GLint name_max_length;\n"
      "    gl->GetProgramiv(program, GL_ACTIVE_UNIFORM_MAX_LENGTH, &name_max_length);\n"
      "\n"
      "    StrBuf::Variable *name = (StrBuf::Variable *) _alloca(StrBuf::Variable::sizeWith(name_max_length));\n"
      "    name->capacity = name_max_length;\n"
      "\n"
      "    GLsizei name_length;\n"
      "    GLint size;\n"
      "    GLenum type;\n"
      "    gl->GetActiveUniform(program, index, name_max_length, &name_length, &size, &type, name->data);\n"
      "    name->length = name_length;\n"
      "\n"
      "    if (size == 1) print(stderr, getUniformTypeName(type), ' ', name->str());\n"
      "    else           print(stderr, getUniformTypeName(type), '[', fmt(size), '] ', name->str());\n"
      "  }\n"
      "\n"
      "  void printProgramResources(GLuint program)\n"
      "  {\n"
      "    {\n"
      "      GLint uniforms_count;\n"
      "      gl->GetProgramInterfaceiv(program, GL_UNIFORM, GL_ACTIVE_RESOURCES, &uniforms_count);\n"
      "\n"
      "      if (uniforms_count)\n"
      "      {\n"
      "        println(stderr, \"    Uniforms:\"_s);\n"
      "      }\n"
      "\n"
      "      for (GLint i = 0; i < uniforms_count; ++i)\n"
      "      {\n"
      "        print(stderr, \"      \"_s, fmt(i, \"2\"), \": \"_s);\n"
      "        printProgramUniformDeclaration(program, i);\n"
      "        println(stderr);\n"
      "      }\n"
      "    }\n"
      "\n"
      "    {\n"
      "      GLint uniform_blocks_count;\n"
      "      gl->GetProgramInterfaceiv(program, GL_UNIFORM_BLOCK, GL_ACTIVE_RESOURCES, &uniform_blocks_count);\n"
      "\n"
      "      if (uniform_blocks_count)\n"
      "      {\n"
      "        println(stderr, \"    Uniform Blocks:\"_s);\n"
      "      }\n"
      "\n"
      "      for (GLint i = 0; i < uniform_blocks_count; ++i)\n"
      "      {\n"
      "        print(stderr, \"      \"_s, fmt(i, \"2\"), \": \"_s);\n"
      "        printProgramResourceName(program, GL_UNIFORM_BLOCK, i);\n"
      "        println(stderr);\n"
      "      }\n"
      "    }\n"
      "\n"
      "    {\n"
      "      GLint storage_blocks_count;\n"
      "      gl->GetProgramInterfaceiv(program, GL_SHADER_STORAGE_BLOCK, GL_ACTIVE_RESOURCES, &storage_blocks_count);\n"
      "\n"
      "      if (storage_blocks_count)\n"
      "      {\n"
      "        println(stderr, \"    Storage Blocks:\"_s);\n"
      "      }\n"
      "\n"
      "      for (GLint i = 0; i < storage_blocks_count; ++i)\n"
      "      {\n"
      "        print(stderr, \"      \"_s, fmt(i, \"2\"), \": \"_s);\n"
      "        printProgramResourceName(program, GL_SHADER_STORAGE_BLOCK, i);\n"
      "        println(stderr);\n"
      "      }\n"
      "    }\n"
      "  }\n"
      "\n"
      "  bool init(gl3w::Functions *gl_functions, bool print_diagnostics = false)\n"
      "  {\n"
      "    bool result = false;\n"
      "    gl = gl_functions;\n"
      "\n"
      "    BREAKABLE_START\n"
      "    {"_s
    );

    bool first = true;

    forEachLinkData (file, files)
    {
      if (first) first = false;
      else println(out);

      Str source = file->data ? *file->data : Str::empty();
      print(out, "\n"
        "      GLuint "_s, file->name, " = compile\n"
        "        (gl, "_s, shaderStage::glEnumName[file->stage],
          ", R\"***("_s, source, ")***\");\n"
        "\n"
        "      if (!"_s, file->name, ") break;\n"
        "      SCOPE_EXIT(gl->DeleteShader("_s, file->name, "));"_s
      );
    }

    forEachLinkData (program, programs)
    {
      print(out, "\n"
        "\n"
        "      // "_s, program->name, "\n"
        "      {"_s
      );

      forEachLinkData (uniform, program->uniforms)
      {
        if (uniform->type->type == typeType::STRUCT)
        {
          print(out, "\n"
            "        gl->GenBuffers(1, &programs::"_s, program->name,
              "::"_s, uniform->name, "::buf);\n"_s
          );
        }
      }

      forEachLinkData (buffer, program->buffers)
      {
        print(out, "\n"
          "        gl->GenBuffers(1, &programs::"_s, program->name,
            "::"_s, buffer->name, "::buf);\n"_s
        );
      }

      print(out, "\n"
        "        programs::"_s, program->name,
          "::program = link(gl, {\n"_s
      );

      forEachEnum (stage, shaderStage)
      {
        ShaderFile *file = program->files[stage];

        if (file) print(out, "          "_s, file->name, ",\n"_s);
      }

      print(out,
        "        });\n"
        "        if (!programs::"_s, program->name, "::program) break;\n"
        "\n"
        "        if (print_diagnostics)\n"
        "        {\n"
        "          print(stderr,\n"
        "            \"Program "_s, program->name, "\\n\"\n"
        "            \"  Expected:\\n\"\n"_s
      );

      first = true;
      forEachLinkData (uniform, program->uniforms)
      {
        if (first)
        {
          first = false;
          print(out,
            "            \"    Uniforms:\\n\"\n"_s
          );
        }

        print(out,
          "            \"      "_s, printTypeDeclaration(uniform->type, uniform->name), "\\n\"\n"_s
        );
      }

      first = true;
      forEachLinkData (buffer, program->buffers)
      {
        if (first)
        {
          first = false;
          print(out,
            "            \"    Buffers:\\n\"\n"_s
          );
        }

        print(out,
          "            \"      "_s, printTypeDeclaration(buffer->type, buffer->name), "\\n\"\n"_s
        );
      }

      print(out,
        "            \"  Actual:\\n\"_s\n"
        "          );\n"
        "\n"
        "          printProgramResources(programs::"_s, program->name, "::program);\n"
        "        }\n"
        "\n"_s
      );

      forEachLinkData (uniform, program->uniforms)
      {
        if (uniform->type->type == typeType::STRUCT)
        {
          print(out, "\n"
            "        if (!bindUniformBuf(gl, programs::"_s, program->name,
              "::program,\n"
            "                                  \""_s, uniform->bindingName, "\"_z,\n"
            "                                  programs::"_s, program->name,
              "::"_s, uniform->name, "::IDX,\n"
            "                                  programs::"_s, program->name,
              "::"_s, uniform->name, "::buf))\n"
            "        {\n"
            "          break;\n"
            "        }\n"_s
          );
        }
        else
        {
          print(out,
            "        programs::"_s, program->name, "::"_s, uniform->name,
              "::offset =\n"
            "          gl->GetUniformLocation(programs::"_s, program->name,
              "::program,\n"
            "                                 \""_s, uniform->bindingName, "\");\n"_s
          );
        }
      }

      forEachLinkData (buffer, program->buffers)
      {
        print(out, "\n"
          "        if (!bindStorageBuf(gl, programs::"_s, program->name,
            "::program,\n"
          "                                  \""_s, buffer->bindingName, "\"_z,\n"
          "                                  programs::"_s, program->name,
            "::"_s, buffer->name, "::IDX,\n"
          "                                  programs::"_s, program->name,
            "::"_s, buffer->name, "::buf))\n"
          "        {\n"
          "          break;\n"
          "        }\n"_s
        );
      }

      print(out,
        "      }"_s
      );
    }

    print(out, "\n"
      "      result = true;\n"
      "    }\n"
      "    BREAKABLE_END\n"
      "\n"
      "    return result;\n"
      "  }\n"
      "\n"
      "  void shutdown()\n"
      "  {"_s
    );

    ///////////////////////////////
    // Print "shutdown" function //
    ///////////////////////////////

    forEachLinkData (program, programs)
    {
      forEachLinkData (uniform, program->uniforms)
      {
        if (uniform->type->type == typeType::STRUCT)
        {
          print(out, "\n"
            "    gl->BindBufferBase(GL_UNIFORM_BUFFER, programs::"_s,
              program->name, "::"_s, uniform->name, "::IDX, 0);\n"_s
            "    gl->DeleteBuffers(1, &programs::"_s, program->name, "::"_s,
              uniform->name, "::buf);\n"_s
          );
        }
      }

      forEachLinkData (buffer, program->buffers)
      {
        print(out, "\n"
          "    gl->BindBufferBase(GL_SHADER_STORAGE_BUFFER, programs::"_s,
            program->name, "::"_s, buffer->name, "::IDX, 0);\n"_s
          "    gl->DeleteBuffers(1, &programs::"_s, program->name, "::"_s,
            buffer->name, "::buf);\n"_s
        );
      }

      print(out, "\n"
        "    gl->DeleteProgram(programs::"_s, program->name, "::program);\n"_s
      );
    }

    print(out,
      "  }\n"
      "}\n"_s
    );
  }
}
